## Authorization Token API

Parameters:
* PWM:
  * client_id: PWMMicroServiceClient
  * client_secret: nMS3wS946DEWeWV2oNFFy54CsWG43EmVPifMAxowzsKYlfB7pkve5MjwL0CQFd5c
  * scope: pwm_api_client
* Entitlements:
  * client_id: Q2MicroServiceClient
  * client_secret: SSTrC0Gk9fCIRyJl9coiNSKKakC7XZhOKcSp0s8LKKyxswvtQ5jrkzyS27MVvnqP
  * scope: q2_api_client

### NP1

#### Request Code

```
curl -X POST \
  'https://cidmsrh.frbnp1.com:3000/as/token.oauth2?grant_type=client_credentials&client_id=PWMMicroServiceClient&client_secret=nMS3wS946DEWeWV2oNFFy54CsWG43EmVPifMAxowzsKYlfB7pkve5MjwL0CQFd5c&scope=pwm_api_client' \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: cidmsrh.frbnp1.com:3000' \
  -H 'Postman-Token: 25f54014-31bd-4a40-b249-673a1697b922,90cf7a0b-522c-4630-9281-ba55b46a7fe1' \
  -H 'User-Agent: PostmanRuntime/7.13.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: ' \
  -H 'cookie: PF=Zl6SvnT4dp0tLs40WiMnai' \
  -b PF=Zl6SvnT4dp0tLs40WiMnai
```

#### Response Example (6/25/2019)

```
{
    "access_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjE1ODU3NTl9.ge1YlduS4XGXdLyocCnaANQH4xF3YZ4xqKBW3Rt7zCybIFsQY_aryi-uNG0Vn_2HTD8hFalnQKKrrU7UuNzt0Cra6YNPiNgA2yKY77OcXEXNn0PJj2fG6WthKqcN7FnUkAN4uZx5IwWI2reskt9w5BjPueljP6u9EEuqzye1lL1BEMWRzGC8VDyRHjtd-SOLe-7293EWMYStJfZ3eXTHz2II9Y3ROyBLDCI7sa_9_9o4fs5QJoWNqXYHGU5AeTSO_b7BMoMwcYHVP40p73SSMDUAmqjcMQ9iitvTzOoM2jf5ZWnmdewUwrsU6Ujr5d3WN45QQP7rX5W1A-JOV4UmyA",
    "token_type": "Bearer",
    "expires_in": 86399
}
```

### NP2

#### Request Code

```
curl -X POST \
  'https://cidmspngfd.corp.frbnp2.com:9031/as/token.oauth2?grant_type=client_credentials&client_id=PWMMicroServiceClient&client_secret=akhcDbkaQb7PfwiQyUjoqwc9jCeRkoYBBahkLGUVR6IRdy2hYlj5ExUiBEuPKY4Q&scope=pwm_api_client' \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: cidmspngfd.corp.frbnp2.com:9031' \
  -H 'Postman-Token: 5ed1d463-9638-40b4-b64b-bae5fe95c07c,e4904866-1b86-4f64-b0f0-b65d5b605c16' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: '
```

#### Response Example (6/20/2019)

```
{
    "access_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjExMzI4Njl9.HMeaRKr92njPcjKU4XjBz4Ry_y8D10YCTaKfuu-HDVclzRv6ixjAwJB1KkSoVqiAfd4_KGclh6WusSLu9c5iiHLNhICb_eDqrbjV52Td9cKEYZmbTBk3VR75pmSICqvKGcrE2zGHPusYDF4j1ui_fz-FleXmGWxwddccC9k_VWqEEv2GS0FRBL3wmu2pfMK3tPN-XQzo3IXTmFGhuc48I5nPDpBK9OaI69R8Kvr3CuPmDy90hy1AD30qcVj3_TzMXQSsyp6rOcKbli53Lq-O4_DjAwO9EPVq6WDYAqkeAQhxzXzYjmQ-UHVp_DKLUZQ9y9SRK0wMVeE7MQiD_H0X3w",
    "token_type": "Bearer",
    "expires_in": 86399
}
```

### NP3

#### Request Code

```
var client = new RestClient("https://cidmspngfd.corp.frbnp3.com:9031/as/token.oauth2?grant_type=client_credentials&client_id=PWMMicroServiceClient&client_secret=sQWvXIUVGL5lfIbFzV8MgdOifvUYQxB68ElcJZohXhkiRxVWUY5WzqF6AmoZGnLX&scope=pwm_api_client");
var request = new RestRequest(Method.POST);
request.AddHeader("cache-control", "no-cache");
request.AddHeader("Connection", "keep-alive");
request.AddHeader("content-length", "");
request.AddHeader("accept-encoding", "gzip, deflate");
request.AddHeader("cookie", "PF=g3jxa1Fiiel77dVfjfLNgU");
request.AddHeader("Host", "cidmspngfd.corp.frbnp3.com:9031");
request.AddHeader("Postman-Token", "ee3d365f-05fe-4df0-8aa9-0bb615676bcd,1afa7af6-f475-4b73-8195-4fed8fd97172");
request.AddHeader("Cache-Control", "no-cache");
request.AddHeader("Accept", "*/*");
request.AddHeader("User-Agent", "PostmanRuntime/7.13.0");
IRestResponse response = client.Execute(request);
```

#### Response Example (5/17/2019)

```
{
    "access_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NTgyMDY0MzF9.cAC9kD0UlF074cq3GTUq2dIdknSBcKYFmdQFaetP0_JgZe0dDGJtpS0SEhRPL9q2LWwms_uAd_Kkka31xFzZL6BTq1jYU0zD4UBTJAeJZhW0Hzs4ZQTUo4zvtQclRIfrFIH1BqLoaRfan_y4SKLHdCO7QGZ_mGMfGz9Sv9SBNdaa2SdR_f-zlmH4Y0y7JVRYASX5naMyRjuNRCbywziwvR7ZMTgihUKsDQCT5JVD5Xd0QzLxTmIB_ddXVBuv-_tXDG_2FMThEbMLAiH2dmjITx6o7_tdygv446Xn6lWQJ2Utu-QnHZGXuAKMCs6CjZyVXRPWHPTeP9OVytrupkMiNw",
    "token_type": "Bearer",
    "expires_in": 86399
}
```
