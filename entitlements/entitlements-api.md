# Entitlements API

- GitHub Repository: https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2
- URLs:
  - NP1: https://dct-nxgn.apps.corp.frbnp1.com/entlmts/v1/entitlements
  - NP2: https://dct-nxgn.corp.frbnp2.com/entlmts/v1/entitlements
  - NP3: https://dct-nxgn.corp.frbnp3.com/entlmts/v1/entitlements
- Swagger UI:
  - NP1: https://dct-nxgn.apps.corp.frbnp1.com:443/entlmts/docs
  - NP2: https://dct-nxgn.corp.frbnp2.com/entlmts/docs
  - NP3: https://dct-nxgn.corp.frbnp3.com/entlmts/docs
- Swagger JSON:
  - NP1: https://dct-nxgn.apps.corp.frbnp1.com:443/entlmts/swagger
  - NP2: https://dct-nxgn.corp.frbnp2.com/entlmts/swagger
  - NP3: https://dct-nxgn.corp.frbnp3.com/entlmts/swagger
- Example LdapIDs to try:
  - 47ac06dd-5099-497b-aea4-0cc6170e221f (from swagger json)
  - 47D90B1C-3255-4C6F-B2C1-C40560CF9345 (1st in txt file)
  - 724600A5-6F22-43A4-B468-9BE8EC819209 (2nd in txt file)
  - More in txt file: https://github.firstrepublic.com/ckwan/wiki/blob/master/ldap-ids-for-testing.txt

## NP1

### Request

```
var client = new RestClient("https://dct-nxgn.apps.corp.frbnp1.com/entlmts/v1/entitlements");
var request = new RestRequest(Method.GET);
request.AddHeader("cache-control", "no-cache");
request.AddHeader("Connection", "keep-alive");
request.AddHeader("accept-encoding", "gzip, deflate");
request.AddHeader("cookie", "67a451442d67ed938e46647ea47f4cbb=220e65804c3eaf0756f5ea540c8c05b1");
request.AddHeader("Host", "dct-nxgn.apps.corp.frbnp1.com");
request.AddHeader("Postman-Token", "1525f69a-b521-4c13-bfeb-5b85eacafcf7,4e238d3d-8e52-4a01-a6c3-425292827a07");
request.AddHeader("Cache-Control", "no-cache");
request.AddHeader("Accept", "*/*");
request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
request.AddHeader("LdapID", "47ac06dd-5099-497b-aea4-0cc6170e221f");
request.AddHeader("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjE4NDYxNTN9.AkTfbzBNsFOOlgxmnsnao_OE91QPjKpgHsLjUhGZUCWnqEAvkMGp1Q1CvbB_sb_IK8FZdEW4631UOEJBEUooQfdX2LAx42f6oBDd3fFezGHl9QXbM6lkPWLZ-aEIs8HivB8a6wAKhMecuW9zzPQz_QbZjCu_gjBm5bwYK1xKwG1spt_9TGw4cqhGMcydbBUFMfVrQRoio5KUmcHbAqocmKAjdYWX99_qqtKV_P_TeqoshlaH8aFZCiZMlff07ewNF1tbMHcAPEimu8PEeLzbd9LXVH4AH7iSpEZ1diGbR8bxHE8qwx09RhMW6qkNLw5gmlnsQhZrGXqvViuMgECGzA");
IRestResponse response = client.Execute(request);
```

### Response (6/28/2019)

```
{
    "correlationId": "71d13c97-9a52-454d-bb83-3ec4c2cebd1c",
    "status": 200,
    "RequestValues": {
        "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f"
    },
    "accounts": [
        {
            "GAID": "226684",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "50003421583",
            "AcctType": "DD",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "z_pwingspan_nextgenauth",
            "OfficerCode": "POI",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "2607",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "604605986",
            "AcctType": "IL",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSIL",
            "AgentID": "z_pwingspan_nextgenauth",
            "OfficerCode": "ABC",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "328078",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "50009243213",
            "AcctType": "DD",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "z_pwingspan_nextgenauth",
            "OfficerCode": "ZXC",
            "Action": "",
            "Note": ""
        }
    ]
}
```

## NP2

### Request (using token from calling Token Server with Entitlements/Q2 client id)

```
var client = new RestClient("https://dct-nxgn.corp.frbnp2.com/entlmts/v1/entitlements");
var request = new RestRequest(Method.GET);
request.AddHeader("cache-control", "no-cache");
request.AddHeader("Connection", "keep-alive");
request.AddHeader("accept-encoding", "gzip, deflate");
request.AddHeader("cookie", "67a451442d67ed938e46647ea47f4cbb=f2c13ef19b1ede7192bee91cdc459b9d");
request.AddHeader("Host", "dct-nxgn.corp.frbnp2.com");
request.AddHeader("Postman-Token", "89fe7d93-7fdd-49d8-a026-2305440ab02f,99a3b77d-5367-40f9-86b4-113b477f72d4");
request.AddHeader("Cache-Control", "no-cache");
request.AddHeader("Accept", "*/*");
request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
request.AddHeader("LdapID", "47ac06dd-5099-497b-aea4-0cc6170e221f");
request.AddHeader("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6Ik5QRV9DRVJUIn0.eyJzY29wZSI6WyJxMl9hcGlfY2xpZW50Il0sImNsaWVudF9pZCI6IlEyTWljcm9TZXJ2aWNlQ2xpZW50IiwiZXhwIjoxNTYxODUxMjg4fQ.fyOGYIEU1XvGih-XmbnQu3cBt_2w0cJMQeB-fe05WiQIe3ytWZxekc8uYyzVtH7_WlvCfLhMYMPTeuTYFpJeeroa7bLmeCaYtG7dpJCqqccH9KSazakHVGnIprAL5Of-Xx7DlisZ6-qn8UnHF1O9OvKphJfJxvZavcfWEW9H76TeTJyJlgYPE5pp9VBSu4CBLpLbqeU2ty758sJl9_GwTQ42kr-nQZ8jHz9DTQaZvVyiKvX9uzty6ht9JG5lMMq3qmBf6G1bP-JpIcdU5GertAAqfsdp4bBAaH_sr47jKy8c-bjQ_Lv9wX0GNTk4fbjYudXcvji0YUcXYP7QWtB1_w");
IRestResponse response = client.Execute(request);
```

### Response (6/28/2019)

```
{
    "correlationId": "688e2709-5934-4824-9c67-e91ff4b4685c",
    "status": 200,
    "RequestValues": {
        "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f"
    },
    "accounts": [
        {
            "GAID": "106539",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "100664581",
            "AcctType": "SV",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSSV",
            "AgentID": "z_np2ssisee01",
            "OfficerCode": "",
            "Action": "",
            "Note": "",
            "ViewAccess": "Y",
            "DebitAccess": "Y",
            "CreditAccess": "Y"
        },
        {
            "GAID": "20836",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "806318877",
            "AcctType": "IL",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSIL",
            "AgentID": "z_np2ssisee01",
            "OfficerCode": "",
            "Action": "",
            "Note": "",
            "ViewAccess": "Y",
            "DebitAccess": "Y",
            "CreditAccess": "Y"
        },
        {
            "GAID": "328078",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "50009243213",
            "AcctType": "DD",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "z_np2ssisee01",
            "OfficerCode": "",
            "Action": "",
            "Note": "",
            "ViewAccess": "Y",
            "DebitAccess": "Y",
            "CreditAccess": "Y"
        },
        {
            "GAID": "226684",
            "LdapID": "47ac06dd-5099-497b-aea4-0cc6170e221f",
            "AcctNumber": "50003421583",
            "AcctType": "DD",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "z_np2ssisee01",
            "OfficerCode": "",
            "Action": "",
            "Note": "",
            "ViewAccess": "Y",
            "DebitAccess": "Y",
            "CreditAccess": "Y"
        }
    ]
}
```

## NP3

### Request

```
var client = new RestClient("https://dct-nxgn.corp.frbnp3.com/entlmts/v1/entitlements");
var request = new RestRequest(Method.GET);
request.AddHeader("cache-control", "no-cache");
request.AddHeader("Connection", "keep-alive");
request.AddHeader("accept-encoding", "gzip, deflate");
request.AddHeader("cookie", "67a451442d67ed938e46647ea47f4cbb=15e927f435ed51a64110af3c484da2fe");
request.AddHeader("Host", "dct-nxgn.corp.frbnp3.com");
request.AddHeader("Postman-Token", "c9d388e7-ebc8-41de-a4c9-8549f6c589d8,484f9a8c-2c80-47ee-9f6b-2bdd71f412b6");
request.AddHeader("Cache-Control", "no-cache");
request.AddHeader("Accept", "*/*");
request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
request.AddHeader("LdapID", "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F");
request.AddHeader("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjE4NDc5Mzl9.bI8KNprk0ZWrBFkCAv-zjP4Q5oSfy7-XhC5tZUAGWLqduxOAOwwlwPsjtCW5PVIL15LrkZuJTSZ_7RHcOd5Pb6UC448wwm2dRACvNMb7rhUc_W6oAapOMHRP6F1zA30ZCkerMdcXedQkOy9-dYbDotXYO8wBy91EP-nGxv0WeG7Nb_kU0vditCDJKHi-8FhWlHH2hQiFlaF0pEX-AHjJ928lzSu5H8LxjjHVMuRFCntFlgynGjISFvI17EMfdIj6EnYsvAKSsHio41wq86VxRY39zYBU7ie8mOCscTkvg4D0nXByndxk-af7E3YCXpUDINcEhkdAS5wCeXrPTBcpcg");
IRestResponse response = client.Execute(request);
```

### Response (6/28/2019)

```
{
    "correlationId": "23d410c2-3377-4ed6-ac14-77e28a67d3d8",
    "status": 200,
    "RequestValues": {
        "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F"
    },
    "accounts": [
        {
            "GAID": "320113",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "802341477",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSIL",
            "AgentID": "anilkumar",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "376945",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "50000348060",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSSV",
            "AgentID": "sgauchan",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "285638",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "99500024551",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "anilkumar",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "382521",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "50000231787",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSSV",
            "AgentID": "sgauchan",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "183356",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "80001789115",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "anilkumar",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "286067",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "99500034147",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "anilkumar",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        },
        {
            "GAID": "285637",
            "LdapID": "4BA4BCB9-8037-4EBF-8CDD-055C881CAE2F",
            "AcctNumber": "99500024510",
            "UltimateParentAcctNumber": "0",
            "NonPersonalGCID": "",
            "SourceName": "TPLUSDD",
            "AgentID": "anilkumar",
            "OfficerCode": "",
            "Action": "",
            "Note": ""
        }
    ]
}
```
