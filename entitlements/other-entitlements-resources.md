These were resources given to me when I was trying to figure out how to access and use the [Entitlements API](entitlements-api.md). They may or may not be relevant to our use case in PWM but are here for reference.

- Offline Swagger YAML file (drop into https://editor.swagger.io/): https://github.firstrepublic.com/FRB/nextgen-service-entitlements/blob/master/src/api/swagger.yaml
- Places I looked when trying to find the URL(s) for the Entitlements API:
  - https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2/blob/0ef0d6e07a96e577f8733f9e467dd6d48b387a74/config/dev.json#L86
    - https://dct-nxgn.apps.corp.frbnp1.com:443/entlmts/v1/entitlements
    - https://dct-nxgn.apps.corp.frbnp1.com/entlmts/v1/entitlements
    - https://dct-nxgn.apps.corp.frbnp1.com/entlmts/v1/readyStatus
  - https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2/blob/master/config/np1.json
  - https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2/tree/master/config
- Example of calling Entitlements API: https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2/blob/0ef0d6e07a96e577f8733f9e467dd6d48b387a74/src/functions/GetEntitlementDetails.js#L24
- Some ldapIds to try with the Entitlements API: https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2/blob/0ef0d6e07a96e577f8733f9e467dd6d48b387a74/test/functions/FilterAccountList/1.entitlementData-properrequest.json
- Example of intersecting Entitlements accounts with MDM accounts: https://github.firstrepublic.com/FRB/nextgen-service-olb-landing-v2/blob/master/src/functions/FilterAccountList.js
