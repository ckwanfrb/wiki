# Links
(in alphabetical order)
* Citrix VDIs (Login with NP admin_ account):
  * NP1: https://np1citrix/Citrix/NP1CUSTOMWeb/
  * NP2: https://np2citrix/Citrix/NP2CUSTOMWeb/
  * NP3: https://np3citrix/Citrix/NP3CUSTOMWeb/
* Eagle Café Boston Menu (must be on FRB computer): https://collaborate.corp.firstrepublic.com/Dept/ADMIN/Pages/EagleCafeBoston.aspx
* GitHub: https://github.firstrepublic.com/ (Login with FRB AD account)
* Jenkins:
  * NP1: https://jenkinsci/job/PWM/ (Login with FRB AD account)
  * twosandbox: https://jenkins-new.twosandbox.com/ (separate login)
* Remote Access to FRB Computer: https://connect.firstrepublic.com
* ServiceNow Requests: https://frb.service-now.com
* Splunk: https://npgssplunk.frbnpgs.com/en-US/app/launcher/home (Login with NP admin_ account)
* OpenShift (Login with NP admin_ account):
  * NP1: https://openshift.corp.frbnp1.com:8443/console/catalog
  * NP2: https://openshift.corp.frbnp2.com:8443/console/catalog
  * NP3: https://openshift.corp.frbnp3.com/console/catalog
* PWM Customer Portal:
  * NP1: https://pwmsvc.corp.frbnp1.com/pwm-customer-portal
  * NP2: https://pwmsvc.corp.frbnp2.com/pwm-customer-portal
  * NP3: https://pwmsvc.corp.frbnp3.com/pwm-customer-portal
* PWM Services:
  * NP1: https://pwmsvc.corp.frbnp1.com/pwm-services
  * NP2: https://pwmsvc.corp.frbnp2.com/pwm-services
  * NP3: https://pwmsvc.corp.frbnp3.com/pwm-services
