# How to find logs in OpenShift in NP1

1. Go to https://np1citrix/Citrix/NP1CUSTOMWeb/ to launch your VDI
    1. Note that VDIs get restarted every night
1. Login with your admin_ credentials (for example, I use adm_datodd)
1. Launch your VDI from the Desktops tab
1. Go to https://openshift.corp.frbnp1.com:8443/console/catalog and login with your admin_credentials
1. You should have access to the pwm-microsvc project: https://openshift.corp.frbnp1.com:8443/console/project/pwm-microsvc/overview
1. Find one of our running pods (e.g. pwm-customer-portal, pwm-services): https://openshift.corp.frbnp1.com:8443/console/project/pwm-microsvc/browse/pods
1. Select the pod and its logs
