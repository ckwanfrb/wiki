# Aggregate Balance API

* Parameters:
  * Test accounts: A9G007875, 33L185403, A9G718927
  * custodianName: “FRSC” 
  * noCache: true

* NP1:
  * Swagger UI: https://pwmsvc.corp.frbnp1.com/wealth-aggregate-balance/v1/docs
  * Base URL: https://pwmsvc.corp.frbnp1.com/wealth-aggregate-balance/v1/
* NP2:
  * Swagger UI: https://pwmsvc.corp.frbnp2.com/wealth-aggregate-balance/v1/docs
  * Base URL: https://pwmsvc.corp.frbnp2.com/wealth-aggregate-balance/v1/
* NP3:
  * Swagger UI: https://pwmsvc.corp.frbnp3.com/wealth-aggregate-balance/v1/docs
  * Base URL: https://pwmsvc.corp.frbnp3.com/wealth-aggregate-balance/v1/

## Request Code

```
curl -X POST "https://pwmsvc.corp.frbnp3.com/wealth-aggregate-balance/v1/" -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjE0NzI4MDF9.gYpBav7mQb8QU-Raf1MuuRxK_YptmYA3JGV_XJj1g0dEaiDqhDzzkRr5K-NwUyVv0b4kJLPtLCXt4LSFrRwpJD2xTqmdSm431wob_uMfzIsW-LTv9iRcOlrYRbxxrgk89h9NFoL-WcPkvEcwpnC-dU2IDEgGkcXSffetcvy2cAG9WZYzkqKaWZm35pgaKx9rj_x88jZi5xY8rLo6xEYq5IbfV_U6_42JmxKtVod1XNBM4RccNjydKbOKAqyk6w59G1eWHPTVh4Iv2cTiQDdzXWQbpDjlt-ok23et2ImFoxUMhnuKyWJbmEfwlBJVEsEYx2B_c5N0hHK7AlODe84wQA" -H "noCache: true" -H "Content-Type: application/json" -d "[ { \"accountNumber\": \"A9G007875\", \"custodianName\": \"FRSC\" }]"
```

## Response Example (6/24/2019)

```
{
  "aggregatedBalances": {
    "records": [
      {
        "accountNumber": "A9G007875",
        "asOfDate": "2019-06-24T15:02:00.000Z",
        "dataSource": "RTAPI",
        "staleIndicator": null,
        "totalAccountValue": 37812.26,
        "accruedInterest": 0,
        "pendingDividend": null,
        "marginBalance": null,
        "dayChangeAmt": 0,
        "dayChangePct": 0,
        "responseObject": {
          "responseCode": 200,
          "responseDescription": "Data available from RTAPI"
        }
      }
    ]
  }
}
```
