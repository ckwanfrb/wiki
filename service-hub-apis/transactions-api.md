# Transactions API (/wealth-client-transactions/v1/)

* GitHub Repository: https://github.firstrepublic.com/PWM/wealth-client-transactions
* NP3
  * API URL: https://pwmsvc.corp.frbnp3.com/wealth-client-transactions/v1/
  * Swagger UI: https://pwmsvc.corp.frbnp3.com/wealth-client-transactions/v1/docs/
  * Swagger JSON: https://pwmsvc.corp.frbnp3.com/wealth-client-transactions/v1/swagger

## Request Code

```
curl -X GET "https://pwmsvc.corp.frbnp3.com/wealth-client-transactions/v1/?startDate=20150101&endDate=20190531" -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjE1ODI5NzR9.BInZJYS3SF6TsuHe5QJw759CXZ-mpG-V-sKAigBesIwanSwa5FzI6di2ECpuhZDfdboPxcl77cNisp_WxXfNwMH1cN9HBSdrRv8PAFT4oedfVpt4jZkFd1va7wtvqyGlI6fqJoQGLKrLqHvSq6M-TfAXl7e5aHfcgwK5751vxKwMpUds2dzn3m87sZbNzm6-1yD4LKB5QBmTRoDlRCCI4JxAkI3cG4z9W6wh9agLvDxayo9ORbS_Dkhpv5CFhIdcUdKAx71uhpwnIVizFS0WTy-fiWxayjF4DJn8IaoUN4AqxgspmP3XszthZAyBTVe5DoUMOT_K1NFblW50azubcQ" -H "noCache: true" -H "accountNumber: List [ "1010000044" ]"
```

## Response Example (DATE)

```
Test
```
