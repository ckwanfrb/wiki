# Account Information API

* NP1
  * Swagger UI:  https://pwmsvc.corp.frbnp1.com/wealth-client-account-information/v1/docs/ 
  * Swagger JSON:  https://pwmsvc.corp.frbnp1.com/wealth-client-account-information/v1/swagger/
  * Sample accounts:
    * 01K000010
    * 01K000011
    * 01K000012
    * 01K000013  
* NP2
  * Swagger UI:  https://pwmsvc.corp.frbnp2.com/wealth-client-account-information/v1/docs/ 
  * Swagger JSON:  https://pwmsvc.corp.frbnp2.com/wealth-client-account-information/v1/swagger/ 
* NP3
  * Swagger UI:  https://pwmsvc.corp.frbnp3.com/wealth-client-account-information/v1/docs/ 
  * Swagger JSON:  https://pwmsvc.corp.frbnp3.com/wealth-client-account-information/v1/swagger/ 
  * NP3:
    * A9G007875 
    * A9G718927
    * 24466414
    * 676618273
    * K19VS00169

## Request Code

```
curl -X GET "https://pwmsvc.corp.frbnp3.com/wealth-client-account-information/v1/" -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkpXVF9DRVJUIn0.eyJzY29wZSI6WyJwd21fYXBpX2NsaWVudCJdLCJjbGllbnRfaWQiOiJQV01NaWNyb1NlcnZpY2VDbGllbnQiLCJleHAiOjE1NjE0NzI4MDF9.gYpBav7mQb8QU-Raf1MuuRxK_YptmYA3JGV_XJj1g0dEaiDqhDzzkRr5K-NwUyVv0b4kJLPtLCXt4LSFrRwpJD2xTqmdSm431wob_uMfzIsW-LTv9iRcOlrYRbxxrgk89h9NFoL-WcPkvEcwpnC-dU2IDEgGkcXSffetcvy2cAG9WZYzkqKaWZm35pgaKx9rj_x88jZi5xY8rLo6xEYq5IbfV_U6_42JmxKtVod1XNBM4RccNjydKbOKAqyk6w59G1eWHPTVh4Iv2cTiQDdzXWQbpDjlt-ok23et2ImFoxUMhnuKyWJbmEfwlBJVEsEYx2B_c5N0hHK7AlODe84wQA" -H "accountNumber: List [ "A9G007875" ]"
```

## Response Example (6/24/2019)

```
{
  "clientAccountInformation": {
    "records": [
      {
        "accountNumber": "A9G007875",
        "custodianName": "FRSC",
        "custodianAccountName": "NEIL W.N. SHEPHERD 2000 TRUST",
        "accountType": "RTRST",
        "taxStatus": "Taxable",
        "accountOpenDate": "2009-09-29",
        "accountCloseDate": null,
        "responseObject": {
          "responseCode": 200,
          "responseDescription": "Data available from PWMDM"
        }
      }
    ]
  },
  "_timestamp": "2019-06-24T14:40:38Z"
}
```
